import importlib
import os
from IPython.display import display
import matplotlib.pyplot as plt
import logging
logger = logging.getLogger(__name__)


def func_constructor(func_configs):
    _module_name = func_configs['module']
    _func_name = func_configs['func']
    if 'params' in func_configs:
        _func_params = func_configs['params']
    else:
        _func_params = dict()

    _module = importlib.import_module(_module_name)
    _func = getattr(_module, _func_name)

    func = _func(**_func_params)
    return func


def make_dir_if_not_exist(pathdir):
    if not os.path.exists(pathdir):
        os.makedirs(pathdir)
    return pathdir


def write_list_to_file(pathdir, lst):
    with open(pathdir, 'w+') as filehandle:
        filehandle.writelines("%s\n" % ele for ele in lst)
    return


def save_plot(fig, savepath=None, figname=None):
    if savepath:
        savepath = make_dir_if_not_exist(savepath)
        fig.savefig(os.path.join(savepath, figname+'.png'), bbox_inches='tight')
        plt.close(fig)
    else:
        display(fig)
