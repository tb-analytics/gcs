import pandas as pd
import numpy as np
import os
import logging
logger = logging.getLogger(__name__)


"""
---------------------
        CLASS
---------------------
"""


class RawDataSet:
    def __init__(self, configs, sample_size=1):
        self._configs = configs
        self.sample_size = sample_size
        self.raw_data = self.load_raw_data().copy()

    @property
    def configs(self):
        return self._configs

    # @property
    # def raw_data_old(self):
    #     df = pd.read_csv(self.configs['filename'], **self.configs['filereadingparams']).fillna(-1)
    #     return df

    def load_raw_data(self):
        # generator of files
        def file_loader(file_dir, files, file_reading_kwargs, sampling):
            # files is a list of dict, with filename and weight within
            if files:
                n = len(files)
            else:
                raise ValueError("[class RawDataSet.raw_data.file_loader] `filenames` cannot be empty!")

            for idx, f in enumerate(files):
                logger.info(f"[class RawDataSet.raw_data.file_loader] Loading {f}, which is {idx+1}/{n} ...")
                file_name = f['filename']
                # if weight not specified, use 1 - idx*0.2 as default, minimum 0.1
                file_weight = f['weight'] if 'weight' in f else max(0.1, (1 - idx*0.2))
                reader = pd.read_csv(os.path.join(file_dir, file_name), chunksize=n * 30000, **file_reading_kwargs)
                for chunk in reader:
                    if sampling:
                        yield chunk.sample(frac=file_weight)
                    else:
                        yield chunk

        rawdata_dir = self.configs['rawdata_dir']
        files = self.configs['filenames']
        sampling = True

        if self.configs['mode'] != 'train':
            if len(self.configs['filenames']) > 1:
                logger.warning("[class RawDataSet.raw_data] Only the first file will be used in score/validate mode.")
                sampling = False
                files = [files[0]]

        df = pd.concat(file_loader(rawdata_dir, files, self.configs['filereadingparams'], sampling),
                       ignore_index=True, join='outer').fillna(-1)

        if self.configs['mode'] == 'train':
            if 0 < self.sample_size < 1:
                logger.info(f"[class RawDataSet.raw_data] Loading datasets with a sample size of {self.sample_size}.")
                df = df.sample(frac=self.sample_size)

        logger.info(f"[class RawDataSet.raw_data.file_loader] Dataset loaded with {df.shape[0]} rows.")

        return df

    @property
    def target_col(self):
        return self.configs['target']

    @property
    def features_cols(self):
        return self.configs['features']['features_cols']

    @property
    def features_value_assertions(self):
        if 'value_assertions' in self.configs['features']:
            return self.configs['features']['value_assertions']
        else:
            return

    def _data_check(self):
        raw_data_columns = self.raw_data.columns

        # all specified columns in the datasets
        for key, col_list in self.features_cols.items():
            assert all(col in raw_data_columns for col in col_list), \
                "[Class RawDataSet._data_check] Feature(s) specified in configs not found in the data."

        # certain columns need to have all and only explicitly specified values
        if self.features_value_assertions is not None:
            for col, values in self.features_value_assertions.items():
                assert sorted(self.raw_data[col].unique().tolist()) == sorted(values), \
                    f"[Class RawDataSet._data_check] The data does not meet value assertions specified in configs." \
                    f"Required: {sorted(values)}, have: {sorted(self.raw_data[col].unique().tolist())}"

        return

    def _data_filters(self, filters):
        _df = self.raw_data.copy()
        mask = (_df.loc[:, list(filters)] == pd.Series(filters)).all(axis=1)
        return _df.loc[mask, :]

    def feature_prep(self, features_subset=None):
        # check data
        self._data_check()
        newly_added_features = list()

        if features_subset is None:
            _df = self.raw_data.copy()
        else:
            _df = self._data_filters(features_subset)

        if 'month_features' in self.features_cols:
            _df, _tmp = cosine_transformation(_df, self.features_cols['month_features'])
            newly_added_features += _tmp

        if 'cat_features' in self.features_cols:
            _df, _tmp = one_hot_encoding(_df, self.features_cols['cat_features'])
            newly_added_features += _tmp

        features = self.features_cols['asis_features'] + newly_added_features

        index = _df[self.features_cols['index']]
        X = _df[features]
        if self.configs['mode'] == 'score':
            y = pd.DataFrame({self.target_col: []})  # empty dataframe
        else:
            y = _df[self.target_col]

        return FeatureSet(index, X, y)


class FeatureSet:
    def __init__(self, index, X, y):
        """
        index, X, y are dataframes with the same # rows
        """
        self._index = index
        self._X = X
        self._y = y

    @property
    def index(self):
        return self._index

    @property
    def X(self):
        return self._X

    @property
    def y(self):
        return self._y

    def balance_sampling(self, sampler, balanced_on=None):
        """
        :param sampler: a sampler uses imblearn API, that has an fit_resample method takes in X, y
               and balanced on y values.
        :param balanced_on: None if balanced on y instance, or a string represents a column name
               which is within index, X, y
        :return: a resampled FeatureSet object
        """
        if balanced_on is None:
            _tmp_sX, new_y = sampler.fit_resample(pd.concat([self.index, self.X], axis=1), self.y)
            new_index = _tmp_sX[self.index.columns]
            new_X = _tmp_sX[self.X.columns]
        else:
            _tmp_df = pd.concat([self.index, self.X, self.y], axis=1)
            assert balanced_on in _tmp_df.columns, \
                "[Class FeatureSet.balance_sampling] The column name does not exist."

            _tmp_s = sampler.fit_resample(_tmp_df, _tmp_df[balanced_on])
            new_index = _tmp_s[self.index.columns].copy()
            new_X = _tmp_s[self.X.columns].copy()
            new_y = _tmp_s[self.y.columns].copy()

        return FeatureSet(new_index, new_X, new_y)

    def remove_multicollinearity(self, threshold=1):
        from scipy.cluster import hierarchy
        from functions.evaluations import plot_feature_correlations
        from collections import defaultdict

        corrdf = self.X.fillna(-1).corr().dropna(axis=1, how='all').dropna(axis=0, how='all')
        corr_linkage = plot_feature_correlations(corrdf)

        cluster_ids = hierarchy.fcluster(corr_linkage, threshold, criterion='distance')
        cluster_id_to_feature_ids = defaultdict(list)
        for idx, cluster_id in enumerate(cluster_ids):
            cluster_id_to_feature_ids[cluster_id].append(idx)
        selected_features = [corrdf.columns[v[0]] for v in cluster_id_to_feature_ids.values()]

        new_X = self.X.loc[:, selected_features].copy()
        return FeatureSet(self.index.copy(), new_X, self.y.copy())

    def align_model_features(self, model_columns):
        cur_columns = self.X.columns.to_list()
        if cur_columns == model_columns:
            return self
        else:
            logger.warning("[Class FeatureSet.align_model_features] The dataset is not aligned with scoring model "
                           "requirements. Missing columns will be set as -1 and extra columns will be dropped. \n"
                           f"model required columns: {model_columns}; \n"
                           f"current feature columns: {cur_columns}. \n")
            new_X = pd.DataFrame(self._X, columns=model_columns).fillna(-1)
            return FeatureSet(self.index.copy(), new_X, self.y.copy())

    def train_test_split(self, **kwargs):
        from sklearn.model_selection import train_test_split as tts
        default_kwargs = {'test_size': 0.3}
        default_kwargs.update(kwargs)
        # split
        X_train, X_test, y_train, y_test = tts(self.X, self.y, **default_kwargs)
        # get index
        index_train = self.index.loc[X_train.index, :]
        index_test = self.index.loc[X_test.index, :]
        return FeatureSet(index_train, X_train, y_train), FeatureSet(index_test, X_test, y_test)


"""
---------------------
      FUNCTIONS
---------------------
"""


def one_hot_encoding(df, cat_columns, drop_first=False):
    df = pd.get_dummies(df, columns=cat_columns, drop_first=drop_first)

    newly_added_columns = []
    for cat in cat_columns:
        tmp = [col for col in df.columns if col.startswith(cat) and '_' in col]
        newly_added_columns += tmp

    return df, newly_added_columns


def cosine_transformation(df, columns, suffix='cos'):
    for col in columns:
        df[col + suffix] = np.cos(df[col])
    newly_added_columns = [c + suffix for c in columns]

    return df, newly_added_columns


def load_data(all_configs, modelname):
    modelconfigs = all_configs['modelspec'][modelname]

    if 'features_subset' in modelconfigs:
        if 'train_sample_size' in modelconfigs['features_subset']:
            try:
                sample_size = float(modelconfigs['features_subset']['train_sample_size'])
                sample_size = max(min(sample_size, 1), 1e-5)  # the number should be 1e-5 ~ 1
            except ValueError:
                logger.warning("[func load_data] `train_sample_size` parameter must be a number. "
                               "Keeping the original records.")
                sample_size = 1

            raw_data = RawDataSet(all_configs, sample_size)
        else:
            raw_data = RawDataSet(all_configs)

        if 'data_filters' in modelconfigs['features_subset']:
            feature_subset_filters = modelconfigs['features_subset']['data_filters']
            features = raw_data.feature_prep(feature_subset_filters)
        else:
            features = raw_data.feature_prep()
    else:
        raw_data = RawDataSet(all_configs)
        features = raw_data.feature_prep()

    return features


def train_test_prep(features, all_configs, modelname):
    from functions.utils import func_constructor
    modelconfigs = all_configs['modelspec'][modelname]
    if 'train_test_split_ratio' in modelconfigs['features_subset']:
        test_size = modelconfigs['features_subset']['train_test_split_ratio']
    else:
        test_size = 0.3
    train, test = features.train_test_split(test_size=test_size)
    if 'train_resampler' in modelconfigs['features_subset']:
        sampler = func_constructor(modelconfigs['features_subset']['train_resampler'])
        train = train.balance_sampling(sampler)
    return train, test

