import os
import glob
import pandas as pd
from functions.utils import make_dir_if_not_exist
# import matplotlib.pyplot as plt
# import numpy as np
# import seaborn as sns
# import sklearn
# from IPython.display import display
import logging

logger = logging.getLogger(__name__)


class GCSegmentation:
    def __init__(self, configs, region):
        self.region = region
        self.dt = configs['dt']
        self.threshold = configs['threshold']
        self.churn_score_files_list = glob.glob(os.path.join(configs['churn_scored_path'], f'{region}_*.csv'))
        self.mb_score_files_list    = glob.glob(os.path.join(configs['mb_scored_path'], f'{region}_*.csv'))
        self.feature_file_path = os.path.join(configs['rawdata_dir'], configs['feature_file_name'])
        self.profile_file_path = os.path.join(configs['rawdata_dir'], configs['profile_file_name'])
        self.filereadingparams = configs['filereadingparams']
        self.na_replacement_value = configs['na_replacement_value']
        self.feature_cols_needed = configs['feature_cols_needed']
        self.churnscore_cols_needed = configs['churnscore_cols_needed']
        self.mb_cols_needed = configs['mb_cols_needed']
        self.seg_df = self.create_minisegments()
        self.segmentation_saving_path = make_dir_if_not_exist(configs['segmentation_files_output_dir'])

    def read_datasets(self, *args, **kwargs) -> pd.DataFrame:
        # read data files
        _feature = pd.read_csv(self.feature_file_path, **self.filereadingparams).fillna(self.na_replacement_value)
        _profile = pd.read_csv(self.profile_file_path, **self.filereadingparams).fillna(self.na_replacement_value)
        _churn_pred = pd.concat((pd.read_csv(f, **self.filereadingparams) for f in self.churn_score_files_list),
                                ignore_index=True)
        _mb_pred = pd.concat((pd.read_csv(f, **self.filereadingparams) for f in self.mb_score_files_list),
                             ignore_index=True)
        _f, _p, _cp, _mbp = _feature[self.feature_cols_needed], _profile, \
                            _churn_pred[self.churnscore_cols_needed], _mb_pred[self.mb_cols_needed]

        # combine datasets together
        df = pd.concat([
            _f[_f['regions'] == self.region].set_index(['sk_goldencustomerid']),
            _p[_p['regions'] == self.region].set_index(['sk_goldencustomerid']).drop('regions', axis=1),
            _cp.rename(columns={'1': 'churn_pos_prob'}).set_index(['sk_goldencustomerid']),
            _mbp.rename(columns={'1': 'mb_pos_prob'}).set_index(['sk_goldencustomerid'])
        ], axis=1).reset_index()

        return df

    def create_segment_quadrant(self, *args, **kwargs) -> pd.DataFrame:
        df = self.read_datasets()

        # reading threshold
        new_churn_threshold = self.threshold[self.region]['churn']['new']
        exs_churn_threshold = self.threshold[self.region]['churn']['existing']
        new_mulby_threshold = self.threshold[self.region]['mb']['new']
        exs_mulby_threshold = self.threshold[self.region]['mb']['existing']

        # adding segment
        df['segment'] = None
        df.loc[(df.definedgroup == 'new') &
               (df.churn_pos_prob >= new_churn_threshold) &
               (df.mb_pos_prob >= new_mulby_threshold), 'segment'] = 'I'
        df.loc[(df.definedgroup == 'new') &
               (df.churn_pos_prob >= new_churn_threshold) &
               (df.mb_pos_prob < new_mulby_threshold), 'segment'] = 'IV'
        df.loc[(df.definedgroup == 'new') &
               (df.churn_pos_prob < new_churn_threshold) &
               (df.mb_pos_prob >= new_mulby_threshold), 'segment'] = 'II'
        df.loc[(df.definedgroup == 'new') &
               (df.churn_pos_prob < new_churn_threshold) &
               (df.mb_pos_prob < new_mulby_threshold), 'segment'] = 'III'
        df.loc[(df.definedgroup == 'existing') &
               (df.churn_pos_prob >= exs_churn_threshold) &
               (df.mb_pos_prob >= exs_mulby_threshold), 'segment'] = 'I'
        df.loc[(df.definedgroup == 'existing') &
               (df.churn_pos_prob >= exs_churn_threshold) &
               (df.mb_pos_prob < exs_mulby_threshold), 'segment'] = 'IV'
        df.loc[(df.definedgroup == 'existing') &
               (df.churn_pos_prob < exs_churn_threshold) &
               (df.mb_pos_prob >= exs_mulby_threshold), 'segment'] = 'II'
        df.loc[(df.definedgroup == 'existing') &
               (df.churn_pos_prob < exs_churn_threshold) &
               (df.mb_pos_prob < exs_mulby_threshold), 'segment'] = 'III'

        return df

    def _north_america_minisegments(self) -> pd.DataFrame:
        df = self.create_segment_quadrant()
        assert all(df['regions'] == 'NA'), "[class GCSegmentation.north_america_minisegment_rules] Only North America " \
                                           "customers can use these rules."
        df['mini_segment'] = None
        for segment_nm in ['I', 'II', 'III', 'IV']:
            if segment_nm == 'I':
                # SEGMENT I
                df.loc[(df['segment'] == segment_nm) & (df['custtype'] == '3-Retained'), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['custtype'] != '3-Retained') &
                       (df['netsalesamt'] >  300), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['custtype'] != '3-Retained') &
                       (df['netsalesamt'] <= 300), 'mini_segment'] = '3'
            elif segment_nm == 'II':
                # SEGMENT II
                df.loc[(df['segment'] == segment_nm) & (df['visits'] >  1), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['visits'] == 1) &
                       (df['netsalesamt'] >  200), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['visits'] == 1) &
                       (df['netsalesamt'] <= 200), 'mini_segment'] = '3'
            elif segment_nm == 'III':
                # SEGMENT III
                df.loc[(df['segment'] == segment_nm) & (df['isomnichannel'] == 1), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['isomnichannel'] == 0) &
                       (df['netsalesamt'] >  300), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['isomnichannel'] == 0) &
                       (df['netsalesamt'] <= 300), 'mini_segment'] = '3'
            elif segment_nm == 'IV':
                df.loc[(df['segment'] == segment_nm), 'mini_segment'] = '1'
            else:
                raise ValueError("[class GCSegmentation.north_america_minisegment_rules] segment_nm should be within "
                                 "'I', 'II', 'III', 'IV'")
        return df

    def _europe_minisegments(self) -> pd.DataFrame:
        df = self.create_segment_quadrant()
        assert all(df['regions'] == 'EU'), "[class GCSegmentation._europe_minisegments] Only Europe " \
                                           "customers can use these rules."
        df['mini_segment'] = None
        for segment_nm in ['I', 'II', 'III', 'IV']:
            if segment_nm == 'I':
                # SEGMENT I
                df.loc[(df['segment'] == segment_nm) & ((df['favchan'] != 'EC') | (df['isomnichannel'] != 0)),
                       'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & ((df['favchan'] == 'EC') & (df['isomnichannel'] == 0))
                       & (df['netsalesamt'] >= 200), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & ((df['favchan'] == 'EC') & (df['isomnichannel'] == 0))
                       & (df['netsalesamt'] <  200), 'mini_segment'] = '3'

            elif segment_nm == 'II':
                # SEGMENT II
                df.loc[(df['segment'] == segment_nm), 'mini_segment'] = '1'
            elif segment_nm == 'III':
                # SEGMENT III
                df.loc[(df['regionleveltourism'] == 'Foreign') & (df['segment'] == segment_nm), 'mini_segment'] = '3'
                df.loc[(df['regionleveltourism'] == 'Domestic') & (df['segment'] == segment_nm) &
                       (df['pctmsrp'] >= 0.6), 'mini_segment'] = '1'
                df.loc[(df['regionleveltourism'] == 'Domestic') & (df['segment'] == segment_nm) &
                       (df['pctmsrp'] <  0.6), 'mini_segment'] = '2'
            elif segment_nm == 'IV':
                df.loc[(df['segment'] == segment_nm), 'mini_segment'] = '1'
            else:
                raise ValueError("[class GCSegmentation._europe_minisegments] segment_nm should be within "
                                 "'I', 'II', 'III', 'IV'")
        return df

    def _apac_minisegments(self) -> pd.DataFrame:
        df = self.create_segment_quadrant()
        assert all(df['regions'] == 'AS'), "[class GCSegmentation._apac_minisegments] Only APAC " \
                                           "customers can use these rules."
        df['mini_segment'] = None
        for segment_nm in ['I', 'II', 'III', 'IV']:
            if segment_nm == 'I':
                # SEGMENT I
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] >= 0.8), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] <  0.8) & (df['custtype'] == '1-New') &
                       (df['visits'] == 1), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] < 0.8) & ((df['custtype'] != '1-New') |
                       (df['visits'] != 1)), 'mini_segment'] = '3'
            elif segment_nm == 'II':
                # SEGMENT II
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] >= 0.8), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] <  0.8), 'mini_segment'] = '2'
            elif segment_nm == 'III':
                # SEGMENT III
                df.loc[(df['segment'] == segment_nm) & (df['ismulticategorybuyer'] == 1), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['ismulticategorybuyer'] == 0) &
                       (df['pctmsrp'] >= 0.8), 'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['ismulticategorybuyer'] == 0) &
                       (df['pctmsrp'] < 0.8), 'mini_segment'] = '3'
            elif segment_nm == 'IV':
                df.loc[(df['segment'] == segment_nm), 'mini_segment'] = '1'
            else:
                raise ValueError("[class GCSegmentation._apac_minisegments] segment_nm should be within "
                                 "'I', 'II', 'III', 'IV'")
        return df

    def _japan_minisegments(self) -> pd.DataFrame:
        df = self.create_segment_quadrant()
        assert all(df['regions'] == 'JP'), "[class GCSegmentation._japan_minisegments] Only Japan " \
                                           "customers can use these rules."
        df['mini_segment'] = None
        for segment_nm in ['I', 'II', 'III', 'IV']:
            if segment_nm == 'I':
                # SEGMENT I
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] >= 0.6), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['pctmsrp'] <  0.6), 'mini_segment'] = '2'
            elif segment_nm == 'II':
                # SEGMENT II
                df.loc[(df['segment'] == segment_nm) & (df['netsalesamt'] >= 300), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['netsalesamt'] <  300), 'mini_segment'] = '2'
            elif segment_nm == 'III':
                # SEGMENT III
                df.loc[(df['segment'] == segment_nm) & (df['netsalesamt'] >= 325), 'mini_segment'] = '1'
                df.loc[(df['segment'] == segment_nm) & (df['netsalesamt'] <  325) & (df['pctmsrp'] >= 0.6),
                       'mini_segment'] = '2'
                df.loc[(df['segment'] == segment_nm) & (df['netsalesamt'] <  325) & (df['pctmsrp'] <  0.6),
                       'mini_segment'] = '3'
            elif segment_nm == 'IV':
                df.loc[(df['segment'] == segment_nm), 'mini_segment'] = '1'
            else:
                raise ValueError("[class GCSegmentation._japan_minisegments] segment_nm should be within "
                                 "'I', 'II', 'III', 'IV'")
        return df

    def create_minisegments(self) -> pd.DataFrame:
        logger.info(f"[class GCSegmentation.create_minisegments] Creating {self.region} mini-segments...")
        if self.region == 'NA':
            return self._north_america_minisegments()
        elif self.region == 'EU':
            return self._europe_minisegments()
        elif self.region == 'AS':
            return self._apac_minisegments()
        elif self.region == 'JP':
            return self._japan_minisegments()
        elif self.region == "All":
            _t = self.create_segment_quadrant()
            _t['mini_segment'] = None
            return _t
        else:
            raise ValueError("[class GCSegmentation.create_minisegments] Region value not valid.")

    def save_minisegments(self):
        df = self.seg_df.copy().round(4)
        df['final_segment'] = df['segment'] + df['mini_segment']
        columns_needed = ['sk_goldencustomerid', 'regions', 'final_segment', 'churn_pos_prob', 'mb_pos_prob']
        saving_path = os.path.join(self.segmentation_saving_path, f'{self.region}_segmentation_{self.dt}.csv')
        logger.info(f"[class GCSegmentation.save_minisegments] Saving {saving_path}...")
        df[columns_needed].to_csv(saving_path, index=False, header=True)
        return
