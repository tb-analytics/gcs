import logging
import argparse
import os
from datetime import date
logger = logging.getLogger(__name__)


def _filenames_handler(keyval_text):
    kvpairs = keyval_text.split('=')
    if len(kvpairs) == 1:
        k = kvpairs[0]
        v = '1'
    elif len(kvpairs) == 2:
        k, v = kvpairs
    else:
        raise ValueError("[func sysargs_parser._filenames_handler] Argument must be a single value or key=val.")

    assert k.endswith('.csv'), "[func sysargs_parser._filenames_handler] File must be in *.csv format."
    assert v.replace('.', '', 1).isdigit(), \
        "[func sysargs_parser._filenames_handler] Weight of a file must be a number."

    weight = float(v)
    assert 0 < weight <= 1, "[func sysargs_parser._filenames_handler] Weight of a file must be within (0, 1]."

    d = dict()
    d['filename'] = k
    d['weight'] = weight
    return d


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def sysargs_parser(arg_lst):
    parser = argparse.ArgumentParser()

    # essential configs for running
    parser.add_argument('--mode', type=str, action='store', dest='mode', default='score',
                        help="Mode for modeling behaviors. [Values] 'score', 'validate', 'train'. "
                             "[Default] 'score'.")
    parser.add_argument('--dt', type=str, action='store', dest='dt', default=date.today().__str__(),
                        help="Date string for the refresh date. [Default] today's date.")
    parser.add_argument('--feature_file_name', type=str, action='store',
                        dest='feature_file_name',
                        help="When mode is 'score'/'validate', this is the feature file name to be used, "
                             "under the rawdata_dir. If not provided, will use `feature_file_prefix`-`dt`.csv.")
    parser.add_argument('--profile_file_name', type=str, action='store',
                        dest='profile_file_name',
                        help="When mode is 'score'/'validate', this is the profile file name to be used, "
                             "under the rawdata_dir. If not provided, will use `profile_file_prefix`-`dt`.csv.")
    parser.add_argument('--filenames', metavar="KEY=VALUE", nargs='+', dest='filenames',
                        help="When mode is 'train', filenames arg is required. If mode is 'score'/'validate', "
                             "this arg will be ignored."
                             "It's a list of key-value pairs of filename and the corresponding weight. "
                             "If weight not specified, use 1 - idx*0.2 as default, minimum 0.1."
                             "Sample: [{'filename': file_name1, 'weight':1}, {'filename': file_name2, 'weight':0.8}]")
    parser.add_argument('--feature_file_prefix', type=str, action='store',
                        dest='feature_file_prefix', default='datasets',
                        help="When mode is 'score'/'validate', if the feature_file_name is missing, "
                             "will use `feature_file_prefix`-`dt`.csv. as feature_file_name. "
                             "[Default] 'datasets'.")
    parser.add_argument('--profile_file_prefix', type=str, action='store',
                        dest='profile_file_prefix', default='profiling',
                        help="When mode is 'score'/'validate', if the profile_file_name is missing, "
                             "will use `profile_file_prefix`-`dt`.csv. as profile_file_name. "
                             "[Default] 'profiling'.")
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="gcs_refresh.log",
                        help="The file location for the log, under the current working directory or absolute path.")

    # configs for filepath
    parser.add_argument('--home_dir', type=str, action='store', dest='home_dir', default='.',
                        help="Parent location where the program runs and all sub-folders created within. "
                             "[Default] current location.")
    parser.add_argument('--rawdata_dir', type=str, action='store', dest='rawdata_dir', default='datasets',
                        help="Relative location with respect to home_dir, indicating where to find feature "
                             "and profile files. [Default] 'datasets'.")
    parser.add_argument('--model_output_dir', type=str, action='store', dest='model_output_dir', default='models',
                        help="Relative location with respect to home_dir, indicating where to save all the "
                             "models. [Default] 'models'.")
    parser.add_argument('--output_dir', type=str, action='store', dest='output_dir', default='output',
                        help="Relative location with respect to home_dir, indicating where to save all the "
                             "results/scores/profiles. [Default] 'output'.")
    parser.add_argument('--scored_files_output_dir', type=str, action='store',
                        dest='scored_files_output_dir', default='scored',
                        help="Relative location with respect to output_dir, indicating where to save all the "
                             "ML model scores. [Default] 'scored'.")
    parser.add_argument('--segmentation_files_output_dir', type=str, action='store',
                        dest='segmentation_files_output_dir', default='segmentation',
                        help="Relative location with respect to output_dir, indicating where to save all the "
                             "segmentation files. [Default] 'segmentation'.")
    parser.add_argument('--profile_files_output_dir', type=str, action='store',
                        dest='profile_files_output_dir', default='profilereports',
                        help="Relative location with respect to output_dir, indicating where to save all the "
                             "profile report files. [Default] 'profilereports'.")

    # optional configs for partial running
    parser.add_argument('--regions', nargs='+', action='store', dest='regions', default=['NA', 'EU', 'AS', 'JP'],
                        help="A list of global regions to be included in the refresh. [Values] a list within"
                             " 'NA', 'EU', 'AS', 'JP'. [Default] all regions.")
    parser.add_argument('--skip_scoring', type=str2bool, action='store',
                        dest='skip_scoring', default=False,
                        help="Whether or not to skip ML scoring. [Values] True/False. [Default] False.")
    parser.add_argument('--run_profile', type=str2bool, action='store',
                        dest='run_profile', default=True,
                        help="Whether or not to run profiling of segments. [Values] True/False. "
                             "[Default] True.")
    parser.add_argument('--standalone_profile', type=str2bool, action='store',
                        dest='standalone_profile', default=False,
                        help="Whether or not to run standalone profile. Use when have a different profile "
                             "to be used for calculation than the one used in segmentation. "
                             "[Values] True/False. [Default] False.")
    parser.add_argument('--all_indexing_profile', type=str2bool, action='store',
                        dest='all_indexing_profile', default=False,
                        help="Whether or not to create image indexing profile of certain KPIs for each segment."
                             " [Values] True/False. [Default] False.")
    parser.add_argument('--index_breakdown_profile', type=str2bool, action='store',
                        dest='index_breakdown_profile', default=False,
                        help="Whether or not to create text breakdown report for certain profile under "
                             "profiledetaillst. [Values] True/False. [Default] False.")

    # parsing args
    args = parser.parse_args(arg_lst)

    # converting all paths to absolute path
    args.rawdata_dir = os.path.join(os.path.realpath(args.home_dir), args.rawdata_dir)
    args.model_output_dir = os.path.join(os.path.realpath(args.home_dir), args.model_output_dir)
    args.output_dir = os.path.join(os.path.realpath(args.home_dir), args.output_dir)
    args.scored_files_output_dir = os.path.join(args.output_dir, args.scored_files_output_dir)
    args.segmentation_files_output_dir = os.path.join(args.output_dir, args.segmentation_files_output_dir)
    args.profile_files_output_dir = os.path.join(args.output_dir, args.profile_files_output_dir)

    # to construct all files:
    # if train, filenames arg is required in key-val pairs. feature_file_name and profile_file_name is not required
    # if validate/score, use feature_file_name and profile_file_name, if missing: use prefix and dt

    if args.mode == 'train':
        if args.filenames is None:
            parser.error("[func sysargs_parser] Train mode requires filenames with corresponding weights.")
        else:
            args.filenames = [_filenames_handler(s) for s in args.filenames]
            if args.run_profile:
                logger.warning("[func sysargs_parser] Train mode doesn't support segmentation and profiling.")
                args.run_profile = False
    else:
        if args.feature_file_name is None:
            args.filenames = [{'filename': f"{args.feature_file_prefix}-{args.dt}.csv",
                               'weight': 1}]
            args.feature_file_name = f"{args.feature_file_prefix}-{args.dt}.csv"
        else:
            args.filenames = [{'filename': args.feature_file_name, 'weight': 1}]

        if args.profile_file_name is None:
            args.profile_file_name = f"{args.profile_file_prefix}-{args.dt}.csv"

    # converting to a dict
    default_configs = vars(args)
    logger.info(f"[func sysargs_parser] Running as below configs:\n{default_configs}")

    return default_configs
