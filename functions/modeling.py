import pandas as pd
import os
from functions.utils import make_dir_if_not_exist
from joblib import dump, load
from functions.utils import func_constructor
from functions.evaluations import ClassificationEval, CumGainLiftEval, PermutationFeatureImportance
import logging
logger = logging.getLogger(__name__)


def train_model(train, test, all_configs, modelname, save_model=True, exec_eval=True):
    def save_pred(data, suffix):
        pred = pd.DataFrame(model.predict_proba(data.X), columns=model.classes_)
        final = pd.concat([data.index.reset_index(drop=True), pred], axis=1)  # reset index to match predictions
        final.to_csv(os.path.join(f"{scored_files_output_dir}", f"{modelname}_{suffix}.csv"), index=False)
        return

    modelconfigs = all_configs['modelspec'][modelname]
    model_output_dir = make_dir_if_not_exist(all_configs['model_output_dir'])
    scored_files_output_dir = make_dir_if_not_exist(
        os.path.join(all_configs['scored_files_output_dir'],
                     all_configs['mode']))
    model_metadata_dir = make_dir_if_not_exist(os.path.join(model_output_dir, f'{modelname}_metadata'))

    model = func_constructor(modelconfigs['base_model'])
    model.fit(train.X, train.y)

    if exec_eval:
        # metadata: evaluation plots, confusion matrix at all threshold
        _ce = ClassificationEval(test, model)
        _ce.plot_classification_evaluation(savepath=model_metadata_dir)
        cm_all_tresh = _ce.confusion_matrix_all_thresholds()
        cm_all_tresh.to_csv(os.path.join(model_metadata_dir, 'cm_all_treshold.csv'), index=False)

        # metadata: gain/lift plot
        _c = CumGainLiftEval(test, model)
        _c.plot_cumulative_gain(savepath=model_metadata_dir)
        _c.plot_cumulative_lift(savepath=model_metadata_dir)
        _c.lgtable.to_csv(os.path.join(model_metadata_dir, 'lift_gain_table.csv'), index=False)

        # metadat: feature importance
        _p = PermutationFeatureImportance(train, model)
        _p.plot_permutation_importance_multicollinearity_removed(savepath=model_metadata_dir)
        _p.permutation_importance().to_csv(os.path.join(model_metadata_dir, 'feat_imp.csv'), index=True)

    # save the model as joblib object
    if save_model:
        model_metadata = {
            'model_columns': train.X.columns.to_list(),
            'model': model
        }

        saved_path = os.path.join(f"{model_output_dir}", f"{modelname}.joblib")
        dump(model_metadata, saved_path)
        logger.info(f"[func train_model] The model saved at {saved_path}!")

    save_pred(train, 'train')
    save_pred(test, 'test')

    return scored_files_output_dir


def score_model(data, all_configs, modelname):
    model_dir = os.path.join(all_configs['model_output_dir'],
                             f"{modelname}.joblib")
    scored_files_output_dir = make_dir_if_not_exist(
        os.path.join(all_configs['scored_files_output_dir'],
                     all_configs['mode']))

    logger.info(f"[func score_model] The model at {model_dir} is loaded for scoring!")
    model_metadata = load(model_dir)
    model = model_metadata['model']
    model_columns = model_metadata['model_columns']
    data = data.align_model_features(model_columns)

    pred = pd.DataFrame(model.predict_proba(data.X), columns=model.classes_)
    final = pd.concat([data.index.reset_index(drop=True), pred], axis=1)  # reset index to match predictions
    final.to_csv(os.path.join(f"{scored_files_output_dir}", f"{modelname}.csv"), index=False)
    return scored_files_output_dir


def validate_model(data, all_configs, modelname):
    model_dir = os.path.join(all_configs['model_output_dir'],
                             f"{modelname}.joblib")
    scored_files_output_dir = make_dir_if_not_exist(
        os.path.join(all_configs['scored_files_output_dir'],
                     all_configs['mode']))
    model_metadata_dir = make_dir_if_not_exist(os.path.join(scored_files_output_dir, f'{modelname}_metadata'))

    logger.info(f"[func validate_model] The model at {model_dir} is loaded for validation!")
    model_metadata = load(model_dir)
    model = model_metadata['model']
    model_columns = model_metadata['model_columns']
    data = data.align_model_features(model_columns)

    # metadata: evaluation plots
    ClassificationEval(data, model).plot_classification_evaluation(savepath=model_metadata_dir)
    _c = CumGainLiftEval(data, model)
    _c.plot_cumulative_gain(savepath=model_metadata_dir)
    _c.plot_cumulative_lift(savepath=model_metadata_dir)

    pred = pd.DataFrame(model.predict_proba(data.X), columns=model.classes_)
    final = pd.concat([data.index.reset_index(drop=True), pred], axis=1)  # reset index to match predictions
    final.to_csv(os.path.join(f"{scored_files_output_dir}", f"{modelname}.csv"), index=False)
    return scored_files_output_dir
