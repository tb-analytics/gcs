from pyspark.sql import SparkSession
import os
import argparse
import logging


def export_to_csv(hive_table_name, dest_name, folder="datasets"):
    if spark.catalog._jcatalog.tableExists(hive_table_name):
        logger.info(f"[func export_to_csv] Start downloading {hive_table_name} ...")
        spark_df = spark.sql(f"select * from {hive_table_name}")
        df = spark_df.toPandas()
        df.columns = [col.lower() for col in df.columns]
        df.to_csv(dest_name, index=False)
        logger.info(f"[func export_to_csv] {hive_table_name} saved at {dest_name}.")
    else:
        logging.error(f"[func export_to_csv] {hive_table_name} does not exist!", stack_info=True)
        raise ValueError()
    return


if __name__ == "__main__":
    spark = SparkSession \
        .builder \
        .enableHiveSupport() \
        .appName("download_hive_table_to_csv") \
        .getOrCreate()

    parser = argparse.ArgumentParser()
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="gcs_refresh.log")
    parser.add_argument('--table_name', type=str, action='store', dest='table_name')
    parser.add_argument('--dest_name', type=str, action='store', dest='dest_name')
    args = parser.parse_args()
    logfile_name = args.logfile_name
    table_name = args.table_name
    dest_name = args.dest_name

    # setup logger
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging.INFO)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    # start downloading files
    logger.info("="*30)
    logger.info("RUNNING download_hive_table_to_csv.py ...")

    export_to_csv(table_name, dest_name)
    
    spark.stop()
