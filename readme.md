# CML Manual Refresh

1. Open a workbench session, in the command prompt in iPython shell (where `$dt` is a date string like '2021-05-01'):
```bash
!sh ./refresh_cml.sh $dt
```

2. The shell script will do the following:
   - Execute `cml_refresh/refresh_feat_prof_data.py`, this script processes `sql/features.hql` and `sql/profile.hql` as text files, and run each command in SparkSQL.
      - End product of this script:
         1. `tb_analytics.dy_gcs_feat_final`
         2. `tb_analytics.dy_gcs_profile_final`
   - Execute `cml_refresh/download_feat_prof_data.py`, download above tables as two csv files, will be save to `datasets/features.csv` and `datasets/profile.csv`.
   - Execute `main.py`, this one use above files to refresh the scoring. All the outputs of the scoring will be saved at `output`. Segmentation files are under `output/segmentation`, and KPIs profile per segment are under `output/profilereports.`
   - Execute `cml_refresh/update_end_tables.py`, will use files from `output` to update tables using SparkSQL.

3. Logging file for all above will be saved at `log/gcs_refresh_$(date +"%Y%m%d%H%M%S").log`.

## Profile history: starting 6/1/2021 profile, all metrics will be R12M basis.**
Impact (below prior to 6/1/2021 was R24M):
- OMNI rate
- Multi-category rate
- Purchased each product category
- Product category newness
