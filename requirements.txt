pyyaml==5.3.1
pandas==1.0.5
matplotlib==3.2.2
ipython==7.16.1
joblib==0.16.0
scikit-learn==0.23.1
numpy==1.18.5
seaborn==0.11.0
openpyxl=3.0.6
