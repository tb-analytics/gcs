DROP TABLE IF EXISTS tb_analytics.dy_gcs_dimlocationinfo;
CREATE TABLE tb_analytics.dy_gcs_dimlocationinfo AS
with locid as (
select dhl.sk_locationid
    , dhl.businessunit
    , substr(dhl.businessunit, 1, 2) as channel
    , case when substr(dhl.businessunit, -2, 2) = 'SN' then 'NA' else substr(dhl.businessunit, -2, 2) end as regions  -- sports as NA
    , g.sk_globalregionid
    , substr(g.globalregiondesc, 5) as globalregiondesc
from cip_analytics.vw_dimhierlocation dhl
join cip_analytics.vw_dimglobalregion g
    on dhl.sk_globalregionid = g.sk_globalregionid
where dhl.sk_channelid = 2 --D2C
    and dhl.sk_locationid <> -1
    and substr(dhl.businessunit, -2, 2) not in ('BR', 'BN', 'CN')  -- exclude Brazil (no sales after 2015), Buddy Shop, and China
)
SELECT locid.*, l.country
FROM locid
JOIN cip_analytics.vw_dimlocation l
    ON locid.sk_locationid = l.sk_locationid
;


DROP TABLE IF EXISTS tb_analytics.dy_gcs_facttransactions;
CREATE TABLE tb_analytics.dy_gcs_facttransactions AS
SELECT p.sk_goldencustomerid
    , cal.daydate as trans_dt
    , cal.calendaryearnum
    , cal.sk_dayid as dayid
    , tr.salereturnvoidflag
    , tr.sk_locationid
    , tr.sk_originatinglocationid
    , l.channel
    , l.country
    , l.regions
    , l.businessunit
    , l.globalregiondesc
    , tr.sk_itemid
    , cast(tr.grosssalesamt as double) grosssalesamt
    , cast(tr.salesaftermarkdownamt as double) salesaftermarkdownamt
    , cast(tr.discountsalesamt as double) discountsalesamt
    , cast(tr.salesunits as double) salesunits
    , cast(tr.grossreturnamt as double) grossreturnamt
    , cast(tr.returnsaftermarkdownamt as double) returnsaftermarkdownamt
    , cast(tr.discountreturnamt as double) discountreturnamt
    , cast(tr.returnunits as double) returnunits
    , tr.ordernumber
FROM cip_analytics.vw_factsalesdetail tr
JOIN cip_analytics.vw_dimparty p
    ON tr.sk_partyid = p.sk_partyid
JOIN cip_analytics.vw_dimgoldencustomer c
    ON p.sk_goldencustomerid = c.sk_goldencustomerid
JOIN cip_analytics.vw_dimday cal
    ON nvl(tr.sk_orderdateid, tr.sk_dayid) = cal.sk_dayid
JOIN tb_analytics.dy_gcs_dimlocationinfo l  -- exclude invalid locations
    ON tr.sk_originatinglocationid = l.sk_locationid
WHERE sk_currencyversionid = 3
    AND salereturnvoidflag <> 'V'
    AND c.rfeflag = 'N'
    AND cal.daydate >= add_months(${CURRENT_DATE}, -24)
    AND cal.daydate < add_months(${CURRENT_DATE}, 12)
;


DROP TABLE IF EXISTS tb_analytics.dy_gcs_dimitemprice;
CREATE TABLE tb_analytics.dy_gcs_dimitemprice as
WITH allitems as (
SELECT di.sk_itemid
    , dc.styledesc
    , dc.stylefamily
    , dc.launchyear
    , dc.seasonname
    -- database only have valid launch data since 2017, will impute unknown launch month as JUN, unknown launch year as 2005
    -- if not specifically marked as unknown values (e.g. -1)
    , to_date(from_unixtime(unix_timestamp(concat('01 ', 
                                                  coalesce(case when dc.launchmonth = '' then NULL else dc.launchmonth end, 'JUN'),
                                                  ' ',
                                                  coalesce(case when dc.launchyear = '' then NULL else dc.launchyear end, '2005')), 'dd MMM yyyy'))) as launchtimestamp
    , to_date(from_unixtime(unix_timestamp(concat('01 ', dc.discontinuemonthyear), 'dd MMM yyyy'))) as discontinuetimestamp
    , case when lower(productcategorydesc) like 'sport%' then 'sport'
           when lower(productcategorydesc) = 'jewelry' then 'jewelry'
           when lower(productcategorydesc) = 'handbags' then 'handbags'
           when lower(productcategorydesc) = 'shoes' then 'shoes'
           when lower(productcategorydesc) = 'small leather goods' then 'slg'
           when lower(productcategorydesc) = 'ready to wear' then 'rtw'
           else 'oth' end as prodcat
FROM cip_analytics.vw_dimitem di
JOIN cip_analytics.vw_dimstylecolor dc
    ON di.sk_stylecolorid = dc.sk_stylecolorid
)
, itemtrans as (
select calendaryearnum, businessunit, sk_itemid
    , round(sum(case when t.salereturnvoidflag = 'S' then t.grosssalesamt else abs(t.grossreturnamt) end) / 
      sum(case when t.salereturnvoidflag = 'S' then t.salesunits else abs(t.returnunits) end), -1) as msrp
from tb_analytics.dy_gcs_facttransactions t
group by calendaryearnum, businessunit, sk_itemid
having sum(case when t.salereturnvoidflag = 'S' then t.salesunits else abs(t.returnunits) end) > 0
)
select ip.sk_itemid
    , ip.calendaryearnum
    , ip.businessunit
    , ip.msrp
    , case when ntile(4) over (partition by ip.calendaryearnum, ip.businessunit, ai.prodcat 
                     order by ip.msrp desc) = 1 then 'High'
           when ntile(4) over (partition by ip.calendaryearnum, ip.businessunit, ai.prodcat 
                     order by ip.msrp desc) = 4 then 'Low'
           else 'Mid' end as pricebucket
    , ai.prodcat
    , ai.styledesc
    , ai.stylefamily
    , ai.launchyear
    , ai.seasonname
    , ai.launchtimestamp
    , ai.discontinuetimestamp
from itemtrans ip
join allitems ai
    on ip.sk_itemid = ai.sk_itemid
;


drop table tb_analytics.dy_gcs_tmp_factsalestransactions;
create table tb_analytics.dy_gcs_tmp_factsalestransactions as
select *
    , coalesce(lead(date_format(trans_dt, 'yyyy-MM-dd')) over (partition by sk_goldencustomerid, sk_itemid order by trans_dt), 
               date_add(trans_dt, 30)) as srchenddt
    , uuid() as suuid
from tb_analytics.dy_gcs_facttransactions
where salereturnvoidflag = 'S';


DROP TABLE IF EXISTS tb_analytics.dy_gcs_factsalestransactions;
create table tb_analytics.dy_gcs_factsalestransactions as
with returnsuuid as (
select suuid
from tb_analytics.dy_gcs_tmp_factsalestransactions t1
left join tb_analytics.dy_gcs_facttransactions t2 
    on t1.sk_goldencustomerid = t2.sk_goldencustomerid
    and t1.sk_itemid = t2.sk_itemid
    and t1.salesunits = t2.returnunits
where t2.trans_dt >= t1.trans_dt 
    and t2.trans_dt < t1.srchenddt
    and t2.salereturnvoidflag = 'R'
    and t1.salereturnvoidflag = 'S'
group by suuid
),
final as (
select s.*, case when r.suuid is null then 'N' else 'Y' end as isreturned
from tb_analytics.dy_gcs_tmp_factsalestransactions s
left join returnsuuid r
    on s.suuid = r.suuid
)
select * from final
;


drop table if exists tb_analytics.dy_gcs_gcid_first_transdt;
create table tb_analytics.dy_gcs_gcid_first_transdt as
select p.sk_goldencustomerid
    , l.regions
    , min(cal.daydate) as firstdt
FROM cip_analytics.vw_factsalesdetail tr
JOIN cip_analytics.vw_dimparty p
    ON tr.sk_partyid = p.sk_partyid
JOIN cip_analytics.vw_dimgoldencustomer c
    ON p.sk_goldencustomerid = c.sk_goldencustomerid
JOIN cip_analytics.vw_dimday cal
    ON nvl(tr.sk_orderdateid, tr.sk_dayid) = cal.sk_dayid
JOIN tb_analytics.dy_gcs_dimlocationinfo l  -- exclude invalid locations
    ON tr.sk_originatinglocationid = l.sk_locationid
WHERE sk_currencyversionid = 3
    AND salereturnvoidflag = 'S'
    AND c.rfeflag = 'N'
GROUP BY p.sk_goldencustomerid, l.regions;


drop table if exists tb_analytics.dy_gcs_feat_gcid;
create table tb_analytics.dy_gcs_feat_gcid as
with output as (
SELECT tr.sk_goldencustomerid
    , tr.regions
    , max(tr.trans_dt) as lastdt
    , datediff(${CURRENT_DATE}, max(tr.trans_dt)) as daystillnow
    , max(case when gc.country is null then 'Unknown'
           when tr.country = gc.country then 'Domestic' 
           else 'Foreign' end) as touristism
FROM tb_analytics.dy_gcs_facttransactions tr
JOIN cip_analytics.vw_dimgoldencustomer gc
    ON tr.sk_goldencustomerid = gc.sk_goldencustomerid
WHERE tr.trans_dt BETWEEN add_months(${CURRENT_DATE}, -12) AND date_add(${CURRENT_DATE}, -1)
    AND salereturnvoidflag = 'S'
GROUP BY tr.sk_goldencustomerid, tr.regions
)
SELECT output.*
    , coalesce(first_date.firstdt, output.lastdt) as firstdt
FROM output
JOIN tb_analytics.dy_gcs_gcid_first_transdt as first_date
    ON output.sk_goldencustomerid = first_date.sk_goldencustomerid
    AND output.regions = first_date.regions
;


drop table if exists tb_analytics.dy_gcs_feat_retainedgcid;
create table tb_analytics.dy_gcs_feat_retainedgcid as
select sk_goldencustomerid, regions
    , case when validvisits = 0 then 1 else validvisits end as validvisits
from (
    select gc.sk_goldencustomerid, gc.regions
        , count(distinct case when t.isreturned = 'N' then t.trans_dt else NULL end) as validvisits
    from tb_analytics.dy_gcs_feat_gcid gc
    join tb_analytics.dy_gcs_factsalestransactions t
    where gc.sk_goldencustomerid = t.sk_goldencustomerid
        and gc.regions = t.regions
        and t.trans_dt > gc.lastdt
        and t.trans_dt < date_add(gc.lastdt, 365)
    GROUP BY gc.sk_goldencustomerid, gc.regions
) x
;


drop table if exists tb_analytics.dy_gcs_feat_nextyrgcidmb;
create table tb_analytics.dy_gcs_feat_nextyrgcidmb as
select sk_goldencustomerid, regions
    , case when validvisits = 0 then 1 else validvisits end as validvisits
    , case when validvisits > 1 then 1 else 0 end as ismultibuyer
from (
    select gc.sk_goldencustomerid, gc.regions
        , count(distinct case when t.isreturned = 'N' then t.trans_dt else NULL end) as validvisits
    from tb_analytics.dy_gcs_feat_gcid gc
    join tb_analytics.dy_gcs_factsalestransactions t
    where gc.sk_goldencustomerid = t.sk_goldencustomerid
        and gc.regions = t.regions
        and t.trans_dt BETWEEN ${CURRENT_DATE} AND date_add(add_months(${CURRENT_DATE}, 12), -1)
    GROUP BY gc.sk_goldencustomerid, gc.regions
) x
;


drop table if exists tb_analytics.dy_gcs_feat_histsalestransactions;
CREATE TABLE tb_analytics.dy_gcs_feat_histsalestransactions as
select t.*
from tb_analytics.dy_gcs_feat_gcid gc
join tb_analytics.dy_gcs_factsalestransactions t
where gc.sk_goldencustomerid = t.sk_goldencustomerid
    and gc.regions = t.regions
    and t.trans_dt <= gc.lastdt
;


drop table if exists tb_analytics.dy_gcs_feat_histtransactions;
CREATE TABLE tb_analytics.dy_gcs_feat_histtransactions as
select t.*
from tb_analytics.dy_gcs_feat_gcid gc
join tb_analytics.dy_gcs_facttransactions t
where gc.sk_goldencustomerid = t.sk_goldencustomerid
    and gc.regions = t.regions
    and t.trans_dt <= gc.lastdt
;


DROP TABLE IF EXISTS tb_analytics.dy_gcs_feat_favchan;
CREATE TABLE tb_analytics.dy_gcs_feat_favchan as
with favchan as (
select sk_goldencustomerid, regions, channel
    , row_number() over (partition by sk_goldencustomerid, regions order by visits desc, netsales desc) as rnk
from (
    select sk_goldencustomerid
        , regions
        , channel
        , count(distinct trans_dt) as visits
        , sum(salesaftermarkdownamt - discountsalesamt) as netsales
    from tb_analytics.dy_gcs_feat_histsalestransactions
    group by sk_goldencustomerid, regions, channel
    ) tmp
)
select sk_goldencustomerid
    , regions
    , channel as favchan
from favchan
where rnk = 1
;


drop table if exists tb_analytics.dy_gcs_feat_tmpfeatures;
create table tb_analytics.dy_gcs_feat_tmpfeatures as
with region_visits as (
SELECT sk_goldencustomerid
    , regions
    , sum(tr.discountsalesamt) / sum(tr.salesaftermarkdownamt) as avgdiscountrate
    , count(distinct case when isreturned = 'N' then trans_dt else NULL end) as visits
    , count(distinct case when isreturned = 'N' then month(trans_dt) else NULL end) as uniquemthvisited
    , count(distinct channel) as chanvisited
    , count(distinct ip.prodcat) as catpurchased
    , count(distinct ip.pricebucket) as pricebucketpurchased
FROM tb_analytics.dy_gcs_feat_histsalestransactions tr
JOIN tb_analytics.dy_gcs_dimitemprice ip
    ON tr.calendaryearnum = ip.calendaryearnum
    and tr.businessunit = ip.businessunit
    and tr.sk_itemid = ip.sk_itemid
GROUP BY sk_goldencustomerid, regions
),
catinfo as (
SELECT tr.sk_goldencustomerid
    , tr.regions
    , ip.prodcat
    , min(tr.trans_dt) as firstcatdt
    , sum(case when tr.isreturned = 'Y' then null else tr.salesaftermarkdownamt - tr.discountsalesamt end) as catspend
FROM tb_analytics.dy_gcs_feat_histsalestransactions tr
JOIN tb_analytics.dy_gcs_dimitemprice ip
    ON tr.calendaryearnum = ip.calendaryearnum
    and tr.businessunit = ip.businessunit
    and tr.sk_itemid = ip.sk_itemid
GROUP BY tr.sk_goldencustomerid, tr.regions, ip.prodcat
),
catpref_tmp as (
SELECT sk_goldencustomerid, regions, prodcat
    , row_number() over (partition by sk_goldencustomerid, regions order by firstcatdt asc) as firstcatdtrnk
    , row_number() over (partition by sk_goldencustomerid, regions order by catspend desc) as catspendrnk
FROM catinfo
),
catpref as (
SELECT c1.*, c2.mostcatspend
FROM (
    SELECT sk_goldencustomerid, regions, prodcat as firstcat
    FROM catpref_tmp
    WHERE firstcatdtrnk = 1
) c1
JOIN (
    SELECT sk_goldencustomerid, regions, prodcat as mostcatspend
    FROM catpref_tmp
    WHERE catspendrnk = 1
) c2
    ON c1.sk_goldencustomerid = c2.sk_goldencustomerid
    AND c1.regions = c2.regions
),
region_chan as (
SELECT regions, count(distinct channel) as chansavail
FROM tb_analytics.dy_gcs_feat_histsalestransactions
GROUP BY regions
)
select rv.sk_goldencustomerid
    , rv.regions
    , case when rv.chanvisited > 1 then 1 else 0 end as isomnichannel
    , case when rv.visits > 1 then 1 else 0 end as ismultibuyer
    , case when rv.catpurchased > 1 then 1 else 0 end as ismulticategorybuyer
    , rv.chanvisited / rc.chansavail as chanpenentration
    , rv.visits
    , rv.uniquemthvisited
    , rv.catpurchased
    , rv.pricebucketpurchased
    , rv.avgdiscountrate
    , cp.firstcat
    , cp.mostcatspend
from region_visits rv
join region_chan rc
    on rv.regions = rc.regions
join catpref cp
    on rv.sk_goldencustomerid = cp.sk_goldencustomerid
    and rv.regions = cp.regions
;


drop table if exists tb_analytics.dy_gcs_feat_tmpfeaturestranssum;
create table tb_analytics.dy_gcs_feat_tmpfeaturestranssum as
with itmkept as (
    select sk_goldencustomerid, regions, count(sk_itemid) as uniqueitmskept 
    from (
        select sk_goldencustomerid, regions, calendaryearnum, sk_itemid
            , sum(salesunits) - sum(returnunits) as unitsonhand
        from tb_analytics.dy_gcs_feat_histsalestransactions
        group by sk_goldencustomerid, regions, calendaryearnum, sk_itemid
    ) as tmp
    where unitsonhand > 0
    group by sk_goldencustomerid, regions
),
trans_info as (
select t.sk_goldencustomerid
    , t.regions
    , count(distinct case when salereturnvoidflag = 'S' then sk_itemid else NULL end) as ttlitemspurchased
    , count(distinct case when salereturnvoidflag = 'R' then sk_itemid else NULL end) as ttlitemsreturned
    , sum(salesunits) as ttlsalesunits
    , sum(returnunits) as ttlreturnunits
    , sum(salesunits) - sum(returnunits) as tllunitskept
    , sum(salesaftermarkdownamt - discountsalesamt) as netsales
    , sum(returnsaftermarkdownamt - discountreturnamt) as netreturns
    , sum(returnsaftermarkdownamt - discountreturnamt) / sum(salesaftermarkdownamt - discountsalesamt) as returnrate
    , sum(salesunits) / count(distinct case when salereturnvoidflag = 'S' then trans_dt else NULL end) as upt
    , sum(grosssalesamt) / sum(salesunits) as msrpaur
    , sum(salesaftermarkdownamt - discountsalesamt) / sum(salesunits) as aur
    , sum(salesaftermarkdownamt - discountsalesamt) / count(distinct case when salereturnvoidflag = 'S' then trans_dt else NULL end) as aov
    , 1 - sum(salesaftermarkdownamt) / sum(grosssalesamt) as avgmkdnrate
    , sum(discountsalesamt) / sum(salesaftermarkdownamt) as avgdsctrate
from tb_analytics.dy_gcs_feat_histtransactions as t
group by t.sk_goldencustomerid, t.regions
)
select t.*
    , i.uniqueitmskept
from trans_info t   
join itmkept i   
    on t.sk_goldencustomerid = i.sk_goldencustomerid
    and t.regions = i.regions
;



DROP TABLE IF EXISTS tb_analytics.dy_gcs_feat_final;
CREATE TABLE tb_analytics.dy_gcs_feat_final as
with sales_dates as (
SELECT sk_goldencustomerid
    , regions
    , trans_dt
FROM tb_analytics.dy_gcs_feat_histsalestransactions
WHERE isreturned = 'N'
GROUP BY sk_goldencustomerid
    , regions
    , trans_dt
),
nextsales as (
SELECT sk_goldencustomerid
    , regions
    , trans_dt
    , coalesce(lead(trans_dt) over (partition by sk_goldencustomerid order by trans_dt asc), trans_dt) as nextsalesdt
FROM sales_dates
),
histdatediff as (
SELECT sk_goldencustomerid, regions
    , avg(datediff(coalesce(nextsalesdt, trans_dt), trans_dt)) as avgpurdatediff
    , max(datediff(coalesce(nextsalesdt, trans_dt), trans_dt)) as longestpurdatediff
FROM nextsales
GROUP BY sk_goldencustomerid, regions
)
SELECT fts.*
    , id.touristism
    , id.daystillnow
    , month(id.firstdt) as acquiredmth
    , month(id.lastdt) as lastpurmth
    , round(datediff(id.lastdt, id.firstdt) / 365, 2) as tenure
    , f.ismultibuyer
    , f.ismulticategorybuyer
    , f.isomnichannel
    , f.chanpenentration
    , fav.favchan
    , f.uniquemthvisited
    , f.visits
    , f.catpurchased
    , f.pricebucketpurchased
    , coalesce(f.avgdiscountrate, 0) as avgdiscountrate
    , coalesce(hd.avgpurdatediff, 0) as avgpurdatediff
    , coalesce(hd.longestpurdatediff, 0) as longestpurdatediff
    , f.firstcat
    , f.mostcatspend
    , case when hd.longestpurdatediff = 0 then 'new' else 'existing' end as definedgroup
    , case when r.sk_goldencustomerid is null then 0 else 1 end as isretained
    , case when r.validvisits is null then 0 else r.validvisits end as validvisits
    , case when rmb.ismultibuyer is null then 0 else rmb.ismultibuyer end as ismultibuyernextyear
    , case when rmb.validvisits is null then 0 else rmb.validvisits end as validvisitsnextyear
FROM tb_analytics.dy_gcs_feat_gcid id
JOIN tb_analytics.dy_gcs_feat_favchan fav
    ON id.sk_goldencustomerid = fav.sk_goldencustomerid
    AND id.regions = fav.regions    
JOIN tb_analytics.dy_gcs_feat_tmpfeatures f  
    ON id.sk_goldencustomerid = f.sk_goldencustomerid
    AND id.regions = f.regions
JOIN tb_analytics.dy_gcs_feat_tmpfeaturestranssum fts  
    ON id.sk_goldencustomerid = fts.sk_goldencustomerid
    AND id.regions = fts.regions
LEFT JOIN histdatediff hd
    ON hd.sk_goldencustomerid = fts.sk_goldencustomerid
    AND hd.regions = fts.regions
LEFT JOIN tb_analytics.dy_gcs_feat_retainedgcid r
    ON id.sk_goldencustomerid = r.sk_goldencustomerid
    AND id.regions = r.regions
LEFT JOIN tb_analytics.dy_gcs_feat_nextyrgcidmb rmb
    ON id.sk_goldencustomerid = rmb.sk_goldencustomerid
    AND id.regions = rmb.regions
;