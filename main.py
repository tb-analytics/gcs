import yaml
import pandas as pd
import sys
import traceback
from functions.preprocessing import *
from functions.modeling import train_model, score_model, validate_model
import logging
from functions.gcs_segments import GCSegmentation


def model_run(default_configs, model_cat):
    with open(os.path.join('configs', f'{model_cat}_config.yaml')) as file:
        configs = yaml.safe_load(file)

    configs.update(default_configs)
    configs['model_output_dir'] = os.path.join(configs['model_output_dir'], model_cat)
    configs['scored_files_output_dir'] = os.path.join(configs['scored_files_output_dir'], model_cat)

    score_saved_path = None
    for modelname in configs['modelspec']:
        try:
            logger.info(f"[func model_run] Processing {configs['mode']} {model_cat} {modelname}...")
            features = load_data(configs, modelname)
            if configs['mode'] == 'train':
                train, test = train_test_prep(features, configs, modelname)
                train_model(train, test, configs, modelname)
            elif configs['mode'] == 'score':
                score_saved_path = score_model(features, configs, modelname)
            elif configs['mode'] == 'validate':
                score_saved_path = validate_model(features, configs, modelname)
            logger.info(f"[func model_run] Done processing {configs['mode']} {model_cat} {modelname}!")
        except Exception as e:
            logger.error(f"[func model_run] {modelname}_{model_cat} failed with error: {e}", exc_info=True)
    return score_saved_path


def modeling(default_configs):
    churn_scored_path = model_run(default_configs, "churn")
    logger.info(f"[func segmentation] Churn files saved at {churn_scored_path}.")
    mb_scored_path = model_run(default_configs, "mb")
    logger.info(f"[func segmentation] MB files saved at {mb_scored_path}.")
    return churn_scored_path, mb_scored_path


def segmentation(default_configs):
    from functions.gcs_segments import GCSegmentation
    if not default_configs['skip_scoring']:
        churn_scored_path, mb_scored_path = modeling(default_configs)
    else:
        # for running profiling purposes
        churn_scored_path = os.path.join(default_configs['scored_files_output_dir'], 'churn', default_configs['mode'])
        mb_scored_path = os.path.join(default_configs['scored_files_output_dir'], 'mb', default_configs['mode'])

    # read seg/profile configs
    with open(os.path.join('configs', f'segmentation_config.yaml')) as file:
        seg_configs = yaml.safe_load(file)
    seg_configs.update(default_configs)

    # if no valid path, exit
    if not bool(churn_scored_path and mb_scored_path):
        raise ValueError("[func segmentation] scored_path not valid!")

    seg_configs['churn_scored_path'] = churn_scored_path
    seg_configs['mb_scored_path'] = mb_scored_path

    for region in default_configs['regions']:
        try:
            logger.info(f"[func segmentation] Creating segmentation for {region}...")
            gcs = GCSegmentation(seg_configs, region)
            gcs.save_minisegments()
            if seg_configs['run_profile']:
                try:
                    profiling(gcs, seg_configs)
                except Exception as e:
                    logger.error(f"[func segmentation] {region} profiling failed with error: {e}.", exc_info=True)
        except Exception as e:
            logger.error(f"[func segmentation] {region} segmentation failed with error: {e}.", exc_info=True)

    return


def profiling(gcs, configs):
    """
    :param gcs: a GCSegmentation object
    :param configs: configs for running profiling
    :return:
    """
    from functions.gcs_profile import SingleSegmentIndexingProfile, AllSegmentsStatsProfile, AllSegmentsIndexingProfile
    from collections import OrderedDict
    from functions.utils import make_dir_if_not_exist

    report_path_miniseg = make_dir_if_not_exist(os.path.join(configs['profile_files_output_dir'], gcs.region))
    profilefunclst = configs['profilefunclst']
    profiledetaillst = configs['profiledetaillst']
    region_minisegment_size = configs['region_minisegment_size']
    df = gcs.seg_df
    region = gcs.region
    dt = gcs.dt

    # === STATS ===
    # MINI-SEGMENTS STATS
    logger.info(f"[func profiling] Creating Mini-Segment Profiles Stats for {region} {dt}...")
    AllSegmentsStatsProfile(df, region, dt, ['segment', 'mini_segment']) \
        .profile_stats_report(report_path_miniseg)

    if configs['index_breakdown_profile']:
        # SPECIFIC BREAKDOWN OF INDEXING
        logger.info(f"[func profiling] Creating Specific Indexing Profiles Stats for {region} {dt}...")
        AllSegmentsIndexingProfile(df, region, dt, ['segment', 'mini_segment']) \
            .profile_text_report(profiledetaillst, report_path_miniseg)

    if configs['all_indexing_profile']:
        # === INDEXING ===
        # MINI-SEGMENTS INDEXING PROFILE
        logger.info(f"[func profiling] Creating Mini-Segment Indexing Profiles for {region} {dt}...")
        for sc in ['I', 'II', 'III', 'IV']:
            sz = region_minisegment_size[region][sc]
            for i in range(1, sz + 1):
                SingleSegmentIndexingProfile(df, region, dt, OrderedDict(segment=sc, mini_segment=str(i))) \
                    .profile_indexing_graph_report(profilefunclst, report_path_miniseg)

    return


def standalone_profile(default_configs):
    from functions.gcs_profile import SingleSegmentIndexingProfile, AllSegmentsStatsProfile, AllSegmentsIndexingProfile
    from collections import OrderedDict
    from functions.utils import make_dir_if_not_exist

    def prep_df(region, dt):
        feat_path = os.path.join(seg_configs['rawdata_dir'], seg_configs['feature_file_name'])
        seg_path = os.path.join(seg_configs['segmentation_files_output_dir'], f'{region}_segmentation_{dt}.csv')
        prof_path = os.path.join(seg_configs['rawdata_dir'], seg_configs['profile_file_name'])

        segmentation_df = pd.read_csv(seg_path, **seg_configs['filereadingparams'])
        feat_df = pd.read_csv(feat_path, **seg_configs['filereadingparams']) \
            .fillna(seg_configs['na_replacement_value'])
        profile_df = pd.read_csv(prof_path, **seg_configs['filereadingparams']) \
            .fillna(seg_configs['na_replacement_value'])

        _f, _p, _s = feat_df.loc[feat_df['regions'] == region, seg_configs['feature_cols_needed']], \
                     profile_df.loc[profile_df['regions'] == region, :], \
                     segmentation_df

        final = pd.concat([
            _f.set_index(['sk_goldencustomerid']),
            _p.set_index(['sk_goldencustomerid']).drop('regions', axis=1),
            _s.set_index(['sk_goldencustomerid']).drop('regions', axis=1),
        ], axis=1, join='inner').reset_index()

        return final

    # read seg/profile configs
    with open(os.path.join('configs', f'segmentation_config.yaml')) as file:
        seg_configs = yaml.safe_load(file)
    seg_configs.update(default_configs)

    profilefunclst = seg_configs['profilefunclst']
    profiledetaillst = seg_configs['profiledetaillst']
    region_minisegment_size = seg_configs['region_minisegment_size']
    dt = seg_configs['dt']

    for region in seg_configs['regions']:
        report_path_miniseg = make_dir_if_not_exist(os.path.join(seg_configs['profile_files_output_dir'], region))
        df = prep_df(region, dt)

        # === STATS ===
        # MINI-SEGMENTS STATS
        logger.info(f"[func standalone_profile] Creating Mini-Segment Profiles Stats for {region} {dt}...")
        AllSegmentsStatsProfile(df, region, dt, ['final_segment']) \
            .profile_stats_report(report_path_miniseg)

        if seg_configs['index_breakdown_profile']:
            # SPECIFIC BREAKDOWN OF INDEXING
            logger.info(f"[func standalone_profile] Creating Specific Indexing Profiles Stats for {region} {dt}...")
            AllSegmentsIndexingProfile(df, region, dt, ['final_segment']) \
                .profile_text_report(profiledetaillst, report_path_miniseg)

        if seg_configs['all_indexing_profile']:
            # === INDEXING ===
            # MINI-SEGMENTS INDEXING PROFILE
            logger.info(f"[func standalone_profile] Creating Mini-Segment Indexing Profiles for {region} {dt}...")
            for sc in ['I', 'II', 'III', 'IV']:
                sz = region_minisegment_size[region][sc]
                for i in range(1, sz + 1):
                    SingleSegmentIndexingProfile(df, region, dt, OrderedDict(segment=sc, mini_segment=str(i))) \
                        .profile_indexing_graph_report(profilefunclst, report_path_miniseg)

    return


if __name__ == "__main__":
    import sys
    from functions.config_parser import sysargs_parser

    # getting default configs
    default_configs = sysargs_parser(sys.argv[1:])

    logfile_name = default_configs["logfile_name"]
    # setup logger
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging.INFO)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    # running scoring/training etc.
    logger.info("=" * 30)
    logger.info("RUNNING main.py ...")

    if default_configs['mode'] == 'train':
        logger.info("[func main] Training mode, only modeling piece will be running.")
        modeling(default_configs)
    else:
        if default_configs['standalone_profile']:
            standalone_profile(default_configs)
        else:
            segmentation(default_configs)
