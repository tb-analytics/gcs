set -e

dtstring=$1
logfile="log/gcs_refresh_$(date +"%Y%m%d%H%M%S").log"

# refresh the data and download features
 python3 cml_refresh/exec_hql_queries.py \
  --filename /home/cdsw/sql/features.hql \
  --queryargs "CURRENT_DATE='${dtstring}'" \
  --logfilename $logfile &&
 python3 cml_refresh/download_hive_table_to_csv.py \
  --logfilename $logfile \
  --table_name "tb_analytics.dy_gcs_feat_final" \
  --dest_name /home/cdsw/datasets/features_$dtstring.csv &&

# refresh the data and download profile
 python3 cml_refresh/exec_hql_queries.py \
  --filename /home/cdsw/sql/profile.hql \
  --queryargs "CURRENT_DATE='${dtstring}'" "CHANNEL_STRING='EC', 'RF', 'RO'" \
  --logfilename $logfile &&
 python3 cml_refresh/download_hive_table_to_csv.py \
  --logfilename $logfile \
  --table_name tb_analytics.dy_gcs_profile_final \
  --dest_name /home/cdsw/datasets/profile_$dtstring.csv &&

# run modeling
python3 main.py --dt ${dtstring} \
    --feature_file_name /home/cdsw/datasets/features_$dtstring.csv \
    --profile_file_name /home/cdsw/datasets/profile_$dtstring.csv \
    --index_breakdown_profile True \
    --logfilename $logfile &&

# update excel reports
python3 cml_refresh/update_excel_report.py --dt ${dtstring} \
  --excel_template_folder /home/cdsw/excel_report_template \
  --profile_folder /home/cdsw/output/profilereports \
  --logfilename $logfile &&

# update end tables
python3 cml_refresh/update_end_tables.py --dt $dtstring \
    --logfilename $logfile \
    --skip_sanity_check True &&

# zip desired files and send
rm -f /home/cdsw/results.zip && 
zip -r -j /home/cdsw/results.zip $logfile \
    /home/cdsw/output/profilereports/excel_reports/*/*_$dtstring.xlsx \
    /home/cdsw/output/segmentation/AS_segmentation_$dtstring.csv \
    /home/cdsw/output/segmentation/EU_segmentation_$dtstring.csv 


echo "==============================" >> $logfile
echo "GCS Refresh Success!" >> $logfile

echo "Success!"
