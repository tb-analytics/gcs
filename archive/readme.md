# Manual Refresh
1. Prepare feature and profile datasets:
    - From the production Hadoop Server run jupyter notebook
    `~/gcs/jupyternotebook/RefreshData-Single.ipynb`
    - Files will be saved at
    `~/gcs/datasets/*-{dt}.csv`
 
 2. Download above files to local (`source/datasets`)
    - `scp -r dyan@nausazrvpcdh20:~/gcs/datasets/*-{dt}.csv .`
 
 3. Active `cltv` conda environment, and run the shell script.
    ```bash
    conda activate cltv
    cd {project_path}/source
    sh refresh.sh {dt}  #no quotes needed
    ```
    - The shell script will create segmentation files at `source/output/segmentation/{region}_segmentation_{dt}.csv`
    - Excel profile reports at `source/output/profilereports/excel_reports/{region}/segprofile_{region}_{dt}.xlsx`
    - This month's profile report summary file at `source/output/profilereports/profile_history.csv`
 
 4. Upload files to Prod Hadoop HDFS
    - All segmentation files `source/output/segmentation/{region}_segmentation_{dt}.csv` to `/tmp/tb_analytics/gcs`
    - Profile report `source/output/profilereports/profile_history.csv` to `/tmp/tb_analytics`
 
 5. Run the incremental update to migration SCD table
    - Jupyter notebook: `~/gcs/jupyternotebook/SCD incremental.ipynb`
 
 6. In Hive, append profile report
    ```sql
    load data inpath '/tmp/tb_analytics/profile_history.csv' into table
    tb_analytics.dy_gcs_profile_history;
    ```
    
 7. In Impala, `invalidate metadata` and `compute stats` for created tables
    - Migration SCD: `tb_analytics.dy_gcs_partyhistory_stg2`
    - Current model scores: `tb_analytics.dy_gcs_current_scores`
    - Profile history: `tb_analytics.dy_gcs_profile_history`
 
 8. Send notification emails to the team and business stakeholders
   - EU: SharePoint [here](https://toryprod.sharepoint.com/:f:/s/CRMEU/ElJasMKX1RRNsvbzZOZBgQ8Bk31fMTFJfsnYhT1nJbD5Fw?e=ZWjAvc)
   - APAC: SharePoint [here](https://toryprod.sharepoint.com/:f:/s/analyticsteam/EpMLXQNkbLFPoCv8fyHPrC0BzCPDgjzO1cwF-6VkoOWpmA?e=72BIA2)
   - NA: Local Share Drive
   - Japan: 