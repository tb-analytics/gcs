set -e

dtstring=$1

python main.py --dt "${dtstring}" --index_breakdown_profile True
python update_excel_report.py --dt "${dtstring}"

echo "Done!"
