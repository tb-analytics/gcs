export PYSPARK_PYTHON=$(which python3)
export DT="2021-05-01"


spark-submit --master yarn \
--driver-memory 3g \
--executor-memory 3g \
--executor-cores 2 \
--conf spark.driver.memoryOverhead=2000 \
--conf spark.executor.memoryOverhead=3000 \
--conf spark.dynamicAllocation.enabled=true \
--conf spark.dynamicAllocation.minExecutors=5 \
--conf spark.dynamicAllocation.maxExecutors=50 \
--conf spark.dynamicAllocation.executorIdleTimeout=300 \
--conf spark.sql.legacy.allowCreatingManagedTableUsingNonemptyLocation=true \
--conf spark.sql.hive.caseSensitiveInferenceMode=NEVER_INFER \
--conf spark.sql.autoBroadcastJoinThreshold=-1 \
spark-job.py --dt $DT > run.log 2>&1 &

tail -f run.log | grep LOG
