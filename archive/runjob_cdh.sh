set -e

dtstring=$1
workingdir="/home/dyan/gcs"
feature_hqlfile="${workingdir}/features.hql"
profile_hqlfile="${workingdir}/profile.hql"
outputfilesdir="${workingdir}/datasets"

echo "Processing '${dtstring}' ..."

flogfile="${workingdir}/logs/feat_hivelog_${dtstring}.log"
hlogfile="${workingdir}/logs/prof_hivelog_${dtstring}.log"

# refresh the data
# for new platform, replace hive with beeline 
hive -hivevar CURRENT_DATE="'${dtstring}'" -f "${feature_hqlfile}" > "${flogfile}" 2>&1 &&

# refresh the data
hive -hivevar CURRENT_DATE="'${dtstring}'" -f "${profile_hqlfile}" > "${hlogfile}" 2>&1 &&

# output the feature file
# for new platform:
# beeline --outputformat=csv2 -e "select xxx" > "xxx.csv"
hive -hiveconf hive.cli.print.header=true \
     -hiveconf hive.resultset.use.unique.column.names=false \
     -e "select * from tb_analytics.dy_gcs_feat_final;" | sed 's/[\t]/,/g;/^WARN/d' > "${outputfilesdir}/datasets-${dtstring}.csv" &&

# output the profile file
hive -hiveconf hive.cli.print.header=true \
     -hiveconf hive.resultset.use.unique.column.names=false \
     -e "select * from tb_analytics.dy_gcs_profile_final;" | sed 's/[\t]/,/g;/^WARN/d' > "${outputfilesdir}/profiling-${dtstring}.csv"

echo "Done!"
