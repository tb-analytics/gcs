import xlwings as xw
import pandas as pd
import argparse
import os
import sys


def is_less_than_1yr(dt_str):
    from datetime import date
    today = date.today()
    refresh_dt = date(*(int(ele) for ele in dt_str.split('-')))
    days_diff = (today - refresh_dt).days
    return days_diff < 365


def update_excel_report(xlsx_filename, region, dt, _df):
    # ==== update the excel reports ===
    # read the workbooks
    wb = xw.Book(xlsx_filename)
    print(f"Updating {region}...")

    # remove next r12m data
    if is_less_than_1yr(dt):
        _df.loc[['actual_retention_rate', 'actual_mb_rate'], :] = None

    # update the sheet
    wb.sheets[raw_sheet_dict[region]].range('A1').value = _df
    wb.sheets[prof_sheet_dict[region]].range('A1').value = f"Date: {dt}"

    # if NA update the mosaic
    if region == 'NA':
        na_mosaic_filename = os.path.join(datafiles_path, region, f"index_breakdown_NA_mosaicprofile_{dt}.csv")
        _df_mosaic = pd.read_csv(na_mosaic_filename, index_col=0, keep_default_na=False)
        wb.sheets[raw_sheet_dict[region]].range('A63').value = _df_mosaic

    print('Processed!')
    wb.save()
    wb.save(os.path.join(datafiles_path, "excel_reports", region, f"segprofile_{region.lower()}_{dt}.xlsx"))
    wb.close()
    return


def prepare_for_histrecord(_df):
    df = _df.T.reset_index().rename({'index': 'segment'}, axis=1)
    df['region'] = region
    df['profile_dt'] = dt + " 00:00:00"
    if 'r12m_netsalesamt_total' not in df.columns:
        df['r12m_netsalesamt_total'] = df['custcnt'] * df['r12m_netsalesamt']
    df['custcnt'] = df['custcnt'].astype(int)

    final = df.loc[:, fields_to_keep]
    return final


fields_to_keep = ['region', 'profile_dt', 'segment', 'custcnt', 'regionalpct',
                'r12m_netsalesamt_total', 'regionalsalespct',
                'churn_pos_prob', 'mb_pos_prob', 'r12m_new_rate', 'r12m_react_rate',
                'r12m_retained_rate', 'actual_retention_rate', 'actual_mb_rate',
                'r12m_rf_rate', 'r12m_ec_rate', 'r12m_ro_rate', 'r12m_omnichannel_rate',
                'r12m_multicategory_rate', 'r12m_majorcatshopped',
                'diff_country_tourists_rate', 'diff_region_tourists_rate',
                'shopped_rtw_rate', 'shopped_slg_rate', 'shopped_shoes_rate',
                'shopped_handbags_rate', 'shopped_jewelry_rate', 'shopped_sport_rate',
                'rtw_newness_med', 'sport_newness_med', 'handbags_newness_med',
                'shoes_newness_med', 'jewelry_newness_med', 'slg_newness_med',
                'r12m_netsalesamt', 'r12m_avgsalesmkdnrate', 'r12m_avgsalesdsctrate',
                'r12m_avgfullpricerate_dollars', 'r12m_avgfullpricerate_units',
                'r12m_bought_fp_only_itm_rate', 'r12m_bought_md_only_itm_rate',
                'r12m_bought_dsct_only_itm_rate', 'r12m_bought_md_x_dsct_itm_rate',
                'r12m_fp_item_rate', 'r12m_md_item_rate', 'r12m_dsct_item_rate',
                'r12m_md_x_dsct_item_rate', 'r12m_fp_only_rate',
                'r12m_offprice_only_rate', 'r12m_mix_price_rate', 'r12m_visits',
                'r12m_ismultibuyers_rate', 'r12m_aov', 'r12m_aur', 'r12m_upt',
                'marketable_rate', 'emailable_rate', 'phoneable_rate', 'mailable_rate',
                'ltv']

parser = argparse.ArgumentParser()
parser.add_argument('--dt', type=str, action='store', dest='dt', required=True)
args = parser.parse_args(sys.argv[1:])
dt = args.dt

datafiles_path = os.path.relpath(os.path.join(os.path.expanduser('~'),
                                 'Documents/Projects/2020 Global Customer Segmentation/source/output/profilereports'))

regions = ['NA', 'EU', 'AS', 'JP']
raw_sheet_dict = {rg: rg.lower() + '_raw' for rg in regions}
prof_sheet_dict = {rg: rg + ' MINISEG CURRENT' for rg in regions}


histrecord_res = []
for region in regions:
    xlsx_filename = os.path.join(datafiles_path, f"segprofile_{region.lower()}.xlsx")
    profile_csv_filename = os.path.join(datafiles_path, region, "report_source_csv",
                                        f"{region}_segmentprofile_{dt}.csv")
    profile_raw = pd.read_csv(profile_csv_filename, index_col=0, keep_default_na=False)

    update_excel_report(xlsx_filename, region, dt, profile_raw.copy())
    histrecord_res.append(prepare_for_histrecord(profile_raw))

print("Saving profile as history...")
prof_hist = pd.concat(histrecord_res)
prof_hist.to_csv(os.path.join(datafiles_path, 'profile_history.csv'), index=False)

